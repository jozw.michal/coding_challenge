Pre-requisites/Set-up
===================================
0. Clone with git clone
1. A virtual environment is recommended - the app runs in Python 3. I have developed it on 3.8.6, so I recommend using this version.
2. Once in virtualenv, install dependencies using "pip install -r requirements.txt" from project's root.
3. Once requirements are installed, run "python setup.py develop" to ensure everything runs smoothly.
4. After setting up, you'll need to initialize the database, for this I have prepared a script under "suade_tests/scripts/init_db.sh"
5. Run the script using "sh init_db.sh", this will create the db tables necessary and migrations. 
6. Set-up done

Running/Usage
===================================
1. Run the project using "python server.py" from project's root.
2. Note that the initial run might take a while, as it auto-populates the database with the test data provided.
3. Project is configured to run on port :5000, feel free to change that in server.py's start function

Querying the endpoint
===================================
1. Project has apidocs, so for a slightly nicer experience go to http://localhost:5000/apidocs/, click on the endpoint and "Try it out", followed by the "Execute" button
2. If the date entered is valid, you should get reponse 200 and a dictionary structured like:
    {
        "customers": 0,
        "total_discount_amount": 0,
        "items": 0,
        "order_total_avg": 0,
        "discount_rate_avg": 0,
        "commissions": {
            "promotions": {},
            "total": 0,
            "order_average": 0
  }
}
3. Invalid dates and any other inputs will yield reponse 400 with an exception message
4. Should you wish to check the endpoint manually (e.g. via cURL) the format is as following:
    http://localhost:5000/api/reports/<dd>/<mm>/<YYYY>
Like:
    http://localhost:5000/api/repors/26/09/2019

Testing
===================================
1. To test, simply go to the suade_test/tests/ directory and execute
    "python -m unittest test_utils.py"
2. Tests prepopulate their database with CSVs from the /test_data/ directory - I havs slightly altered it, so some assertions might look a bit odd.




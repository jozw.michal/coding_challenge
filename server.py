from flask import Flask
from suade_test.endpoints import ma, swagger
from flask_migrate import Migrate
from suade_test.models import db, prepopulate_db
migrate = Migrate()


def init_app():
    app = Flask(__name__)
    app.config['SWAGGER'] = {'openapi': '3.0.2'}
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
    app.url_map.strict_slashes = False

    ma.init_app(app)
    swagger.init_app(app)
    db.init_app(app)
    migrate.init_app(app, db)

    with app.app_context():
        db.create_all()
        prepopulate_db(db)
        from suade_test.endpoints.v1 import api_bp as api_bp1
        app.register_blueprint(api_bp1, url_prefix='/api')

    return app


app = init_app()


def start():
    app.run(debug=app.config['DEBUG'], host='0.0.0.0', port=5000)


if __name__ == '__main__':
    start()

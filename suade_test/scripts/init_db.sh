WD=`dirname $0`
CWD=`pwd`

echo -n "Initializing DB..."
cd $WD/../
FLASK_APP=server.py flask db init
FLASK_APP=server.py flask db migrate
FLASK_APP=server.py flask db upgrade
cd $CWD
echo "Done" 

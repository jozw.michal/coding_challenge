from flask_marshmallow import Marshmallow
from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from flasgger import Swagger

ma = Marshmallow()
swagger = Swagger()

spec = APISpec(
    title="Suade Test",
    version="1.0.0",
    openapi_version="3.0.2",
    info=dict(description="Suade Test"),
    plugins=[MarshmallowPlugin()],
)

swagger.template = spec.to_dict()

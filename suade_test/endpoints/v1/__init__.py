from flask import Blueprint
from flask_restful import Api
from .report import Report

api_bp = Blueprint('api_v1', __name__)
api = Api(api_bp)

api.add_resource(
    Report,
    '/reports/<day>/<month>/<year>'
)

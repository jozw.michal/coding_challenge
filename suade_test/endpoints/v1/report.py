from flask import abort
from flask_restful import Resource
from datetime import datetime
from suade_test.utils.reports import generate_report


class Report(Resource):
    def get(self, day, month, year):
        """
        Get sales report for a given date
        ---
        parameters:
          - in: path
            name: day
            description: Day to generate data for
            schema:
                type: string
            required: true
          - in: path
            name: month
            description: Month to generate data for
            schema:
                type: string
            required: true
          - in: path
            name: year
            description: Year to generate data for
            schema:
                type: string
            required: true
        responses:
            200:
                description: Response
        """
        try:
            datetime_str = '{}/{}/{}'.format(day, month, year)
            date = datetime.strptime(datetime_str, "%d/%m/%Y")
            result = generate_report(date)
        except Exception as e:
            abort(400, str(e))

        return result, 200

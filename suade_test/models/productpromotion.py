from . import db


class ProductPromotion(db.Model):
    __tablename__ = 'ProductPromotion'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    date = db.Column(db.DateTime)
    product_id = db.Column(db.Integer, db.ForeignKey('Product.id'))
    promotion_id = db.Column(db.Integer, db.ForeignKey('Promotion.id'))

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

from . import db
from sqlalchemy.orm import relationship
from sqlalchemy.ext.associationproxy import association_proxy


class OrderLine(db.Model):
    __tablename__ = 'OrderLine'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    order_id = db.Column(db.Integer, db.ForeignKey('Order.id'))
    order = relationship('Order')
    product_id = db.Column(db.Integer, db.ForeignKey('Product.id'))
    product = relationship('Product')
    product_description = association_proxy('product', 'description')
    product_price = db.Column(db.Float)
    product_vat_rate = db.Column(db.Float)
    discount_rate = db.Column(db.Float)
    quantity = db.Column(db.Integer)
    full_price_amount = db.Column(db.Float)
    discounted_amount = db.Column(db.Float)
    vat_amount = db.Column(db.Float)
    total_amount = db.Column(db.Float)

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

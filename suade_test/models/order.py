from . import db


class Order(db.Model):
    __tablename__ = 'Order'

    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime)
    vendor_id = db.Column(db.Integer)
    customer_id = db.Column(db.Integer)

    def __init__(self, **kwargs):
        self.__dict__.update(**kwargs)

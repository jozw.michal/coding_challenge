from . import db


class VendorCommissions(db.Model):
    __tablename__ = 'Commissions'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    date = db.Column(db.DateTime)
    vendor_id = db.Column(db.Integer)
    rate = db.Column(db.Float)

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

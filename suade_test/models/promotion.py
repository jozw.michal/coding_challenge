from . import db


class Promotion(db.Model):
    __tablename__ = 'Promotion'

    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String)

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

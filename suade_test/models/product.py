from . import db


class Product(db.Model):
    __tablename__ = 'Product'

    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String)

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

from flask_sqlalchemy import SQLAlchemy
import csv
import os
from dateutil import parser

db = SQLAlchemy()

from .order import Order
from .orderline import OrderLine
from .product import Product
from .productpromotion import ProductPromotion
from .promotion import Promotion
from .vendorcommissions import VendorCommissions

basedir = os.path.abspath(os.path.dirname(__file__))


def prepopulate_db(conn, dir_path=None):
    def _check_exists(model, db_model):
        if db.session.query(
            model
           ).filter(model.id == db_model.id).first() is not None:
            return True
        return False

    if dir_path is None:
        dir_files = os.listdir(os.path.join(basedir, 'data'))
        dir_path = os.path.join(basedir, 'data')
    else:
        dir_files = os.listdir(dir_path)

    for file_name in dir_files:
        f = open(os.path.join(dir_path, file_name), 'r')
        csv_file = csv.DictReader(f, delimiter=',') # noqa 

        for row in csv_file:
            parsed_data = {}
            for key, value in row.items():
                parsed_data[key] = value
                if key == 'created_at' or key == 'date':
                    parsed_data[key] = parser.parse(value)

            exists = False
            if 'commissions' in file_name:
                db_model = VendorCommissions(**parsed_data)
                exists = _check_exists(VendorCommissions, db_model)

            elif 'order_lines' in file_name:
                db_model = OrderLine(**parsed_data)
                exists = _check_exists(OrderLine, db_model)

            elif 'orders' in file_name:
                db_model = Order(**parsed_data)
                exists = _check_exists(Order, db_model)

            elif 'product_promotions' in file_name:
                db_model = ProductPromotion(**parsed_data)
                exists = _check_exists(ProductPromotion, db_model)

            elif 'product' in file_name:
                db_model = Product(**parsed_data)
                exists = _check_exists(Product, db_model)

            elif 'promotions' in file_name:
                db_model = Promotion(**parsed_data)
                exists = _check_exists(Promotion, db_model)

            """
            If any record whatsoever exists for a model just return, this is only for
            convenience/testing purposes, so I'll only check the 1st row
            of any file - there are smarter ways of prepopulating dbs
            """

            if exists:
                return
            else:
                conn.session.add(db_model)
                conn.session.commit()
        f.close()

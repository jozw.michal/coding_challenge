class _dot(dict):
    """
    Dict extension to allow us to grab dict values
    using the dot operator
    """
    __getattr__ = dict.get


def make_fake_dict(key, value, amount):
    """
    Small helper so we don't have to
    process orderLines just to get averages
    etc.
    """
    fake_dict_list = []
    for i in range(amount):
        fake_dict_list.append(_dot({
            key: value
        }))
    return fake_dict_list

import unittest
from flask import Flask
from datetime import datetime
from suade_test.models import prepopulate_db
from suade_test.models import db
from helpers.helpers import make_fake_dict
from suade_test.utils.db_utils import (calculate_discount_amount,
                                       calculate_discount_rate_avg, calculate_num_items,  # noqa
                                       calculate_unique_cust_for_day,
                                       get_orders_for_day)


class TestUtils(unittest.TestCase):

    def setUp(self):
        """
        Create new test db with data from test_data dir
        """
        self.test_date = datetime(year=2019, month=9, day=29)
        self.app = Flask(__name__)
        db.init_app(self.app)
        with self.app.app_context():
            db.create_all()
            prepopulate_db(db, dir_path='../tests/test_data')

    def tearDown(self):
        """
        Get rid of db once tests finished
        """
        with self.app.app_context():
            db.drop_all()

    def test_gets_unique_customers(self):
        with self.app.app_context():
            self.assertEqual(calculate_unique_cust_for_day(self.test_date), 3)

    def test_gets_orders_for_day_correct(self):
        with self.app.app_context():
            orders = get_orders_for_day(self.test_date)
            for order in orders:
                self.assertEqual(order.created_at.day, self.test_date.day)
                self.assertEqual(order.created_at.month, self.test_date.month)
                self.assertEqual(order.created_at.year, self.test_date.year)

    def test_gets_unique_orders(self):
        with self.app.app_context():
            orders = get_orders_for_day(self.test_date)
            self.assertEqual(orders.count(), 5)
            orders = get_orders_for_day(self.test_date, True)
            self.assertEqual(orders.count(), 3)

    def test_calculates_discount_rate_avg(self):
        fake_order_lines = make_fake_dict("discount_rate", 1.0, 3)
        with self.app.app_context():
            self.assertEqual(calculate_discount_rate_avg(fake_order_lines), 1.0) # noqa

    def test_calculates_num_items(self):
        fake_order_lines = make_fake_dict("quantity", 100, 3)
        with self.app.app_context():
            self.assertEqual(calculate_num_items(fake_order_lines), 300)

    def test_calculates_discount_amount(self):
        fake_order_lines = make_fake_dict("discounted_amount", 0.5, 3)
        with self.app.app_context():
            self.assertEqual(calculate_discount_amount(fake_order_lines), 1.5)

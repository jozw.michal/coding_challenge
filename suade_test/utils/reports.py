from .db_utils import (calculate_discount_amount, calculate_discount_rate_avg,
                       calculate_num_items, calculate_order_total_avg,
                       calculate_unique_cust_for_day, filter_order_lines_by_date, # noqa
                       calculate_promotions)


def generate_report(date):
    order_lines = filter_order_lines_by_date(date)
    promotions, total, avg = calculate_promotions(date)
    for key in promotions:
        key = str(key)

    return {
        "customers": calculate_unique_cust_for_day(date),
        "total_discount_amount": calculate_discount_amount(order_lines),
        "items": calculate_num_items(order_lines),
        "order_total_avg": calculate_order_total_avg(date, order_lines),
        "discount_rate_avg": calculate_discount_rate_avg(order_lines),
        "commissions": {
            "promotions": promotions,
            "total": total,
            "order_average": avg
        }
    }

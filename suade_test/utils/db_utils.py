from suade_test.models.order import Order
from suade_test.models.orderline import OrderLine
from suade_test.models.vendorcommissions import VendorCommissions
from suade_test.models.productpromotion import ProductPromotion
from suade_test.models import db
from sqlalchemy import extract


def get_orders_for_day(date, customer_unique=False):
    orders = Order.query.filter(
        extract('day', Order.created_at) == date.day,
        extract('month', Order.created_at) == date.month,
        extract('year', Order.created_at) == date.year
    )

    if customer_unique:
        return orders.group_by(Order.customer_id)

    return orders


def filter_order_lines_by_date(date):
    orders = get_orders_for_day(date).all()
    order_lines_day = []
    for order in orders:
        order_line = OrderLine.query.filter(
            OrderLine.order_id == order.id
            ).all()
        order_lines_day += order_line

    return order_lines_day


def calculate_unique_cust_for_day(date):
    unique_cust = get_orders_for_day(date, customer_unique=True).all()
    filter_order_lines_by_date(date)
    return len(unique_cust)


def calculate_discount_rate_avg(order_lines):
    items_len = len(order_lines)
    discount_rate_total = 0
    for order in order_lines:
        discount_rate_total += order.discount_rate

    if discount_rate_total > 0:
        discount_rate_avg = discount_rate_total/items_len
        return round(discount_rate_avg, 2)

    return 0


def calculate_order_total_avg(date, order_lines):
    num_orders = get_orders_for_day(date).count()
    order_total = 0
    for order in order_lines:
        order_total += order.total_amount

    if order_total > 0:
        order_avg = order_total/num_orders
        return round(order_avg, 2)

    return 0


def calculate_num_items(order_lines):
    num_items = 0
    for order in order_lines:
        num_items += order.quantity

    return num_items


def calculate_discount_amount(order_lines):
    total_discount = 0
    for order in order_lines:
        total_discount += order.discounted_amount

    return round(total_discount, 2)


def calculate_promotions(date):
    """
    Not super happy with this, creating joins in ORMs, I still find rather
    painful but running out of time so fingers crossed here lol
    """
    joined_query = db.session.query(
        Order, OrderLine, VendorCommissions, ProductPromotion
    ).join(
        VendorCommissions, VendorCommissions.vendor_id == Order.vendor_id
    ).join(
        OrderLine, OrderLine.order_id == Order.id
    ).join(
        ProductPromotion, ProductPromotion.product_id == OrderLine.product_id
    ).filter(
        extract('day', VendorCommissions.date) == date.day,
        extract('month', VendorCommissions.date) == date.month,
        extract('year', VendorCommissions.date) == date.year
    ).filter(
        extract('day', Order.created_at) == date.day,
        extract('month', Order.created_at) == date.month,
        extract('year', Order.created_at) == date.year
    ).filter(
        extract('day', ProductPromotion.date) == date.day,
        extract('month', ProductPromotion.date) == date.month,
        extract('year', ProductPromotion.date) == date.year
    ).group_by(OrderLine.id)

    promotions = {}
    total = 0
    for data in joined_query:
        key = data.ProductPromotion.promotion_id
        commission = round(
            data.OrderLine.total_amount * data.VendorCommissions.rate, 2
            )
        if data.ProductPromotion.promotion_id not in promotions.keys():
            promotions[key] = commission
        else:
            promotions[key] = promotions[key] + commission
        total += commission

    avg = 0
    if joined_query.count() > 0:
        avg = total / joined_query.count()

    return promotions, round(total, 2), round(avg, 2)
